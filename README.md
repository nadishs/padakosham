# Padakosham

A 2-day hackathon organized by IT@School, KochiPython and Swathanthra Malayalam Computing (SMC).
IT@School is working on a new initiative to augment the IT Education at school, and a web-application is crucial to get it off the ground. To build and deploy this project, KochiPython and SMC have teamed up with IT@School to conduct a hackathon over a weekend. 

Anyone with a moderate level of experience with Django can register and participate in this hackathon.

More Details:



IT@School is starting IT Clubs in schools all across Kerala.  One of the major activities being planned is building a Malayalam - Malayalam dictionary (which may be extended to a multi-lingual dictionary in the future). This project is designed to pique the interest of students in software products while also being a useful project for the general public. This would be an ideal project to build a data-rich, crowd-source dictionary and, if executed properly, this could act as a cornerstone for other Malayalam computing applications. 

## Hackathon

* Event starts at 10 AM on 25th March (Saturday) and ends at 12 PM on 26th March 2017 (Sunday)
* Venue: IT@School RRC, Edappalli, Ernakulam
* Food and refreshments would be provided at the venue, free of cost, for the participants.
* RRC Center is well-equipped if you need to crash for a few hours during the hackathon.
* For Hackathon Instructions, [click here](https://gitlab.com/smc/padakosham/wikis/hackathon-instructions)
* [FAQs](hackathon-faq)